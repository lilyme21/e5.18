/////////////////////////////////////////////////////////////
// Lily Mendoza
// CS 49J
// 9/17/19
// This program takes a user input integer as an amount and
// calculates the tax based on how high the amount is. It then
// prints out the final amount after tax as a string.
/////////////////////////////////////////////////////////////

import java.text.DecimalFormat;
import java.util.Scanner;       // importing package to use Scanner

public class E5_18 {
    public static void main(String[] args) {
        // prompting user to input an income amount
        System.out.println("Please input an income amount in numbers only: \n $");
        // scanning input and saving in variable amt
        Scanner in = new Scanner(System.in);
        int amt = in.nextInt();
        // initializing taxAmount variable
        int taxAmount = 0;
        // calculating tax by income amount
        if (amt > 500000)
            taxAmount = (amt * 6/100) + amt;
        else if (amt > 250000)
            taxAmount = (amt * 5/100) + amt;
        else if (amt > 100000)
            taxAmount = (amt * 4/100) + amt;
        else if (amt > 75000)
            taxAmount = (amt * 3/100) + amt;
        else if (amt > 50000)
            taxAmount = (amt * 2/100) + amt;
        else
            taxAmount = (amt/100) + amt;
        // converting amount to money string
        DecimalFormat money = new DecimalFormat("#,###,###,###");
        // printing taxed amount
        System.out.println("With income tax: " + money.format(taxAmount));

    }

}
